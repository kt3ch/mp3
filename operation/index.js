const url = require("url");
const mongoose = require("mongoose");
const User = mongoose.model("User");
const Task = mongoose.model("Task");
const ObjectID = require('mongodb').ObjectID;

parseQuery = (condition, result_agent) => {
  if (condition.where) {
    result_agent.where(JSON.parse(condition.where));
  }
  if (condition.sort) {
    result_agent.sort(JSON.parse(condition.sort))
    // result.sort(condition.sort);
  }
  if (condition.select) {
    result_agent.select(JSON.parse(condition.select))
  }
  if (condition.skip) {
    result_agent.skip(JSON.parse(condition.skip))
  }
  if (condition.limit) {
    result_agent.limit(JSON.parse(condition.limit))
  }
  if (condition.count) {
    result_agent.count(JSON.parse(condition.count))
  }
  // if (condition.filter) {
  //   result_agent.filter(JSON.parse(condition.filter))
  // }
  // return result_agent;
};

exports.createUser = async (request, response) => {
  let body = request.body;
  if(body.name === null || body.name === "") {
    response.status(403).json({
      message: "Users cannot be created (or updated) without a name",
      data: []
    });
    return;
  }
  if(body.email === null || body.email === "") {
    response.status(403).json({
      message: "Users cannot be created (or updated) without an email",
      data: []
    });
    return;
  }
  if (body.pendingTasks && body.pendingTasks.length > 0) {
    for (let i = 0; i < body.pendingTasks.length; i++) {
      let add = false;
      let tid = ObjectID(body.pendingTasks[i]);
      await Task.findById(tid).exec().then(retval => {
        if (retval && !retval.completed) {
          add = true
        }
      })
        .catch(error => {

        });
      if (add) {
        continue;
      }
      return response.status(500).json({
        message : "Server Error: add error",
        data : []
      })
    }
    body.pendingTasks = body.pendingTasks.filter((v, i, a) => a.indexOf(v) === i);
  }
  let user = new User(body);
  await user.save().then(async retval => {
    for (let i = 0; i < retval.pendingTasks.length; i++) {
      let add = false;
      let tid = ObjectID(retval.pendingTasks[i]);
      await Task.findByIdAndUpdate(tid, {assignedUser: retval._id, assignedUserName: retval.name}).exec().then(retval => {
        if (retval) {
          add = true
        }
      })
        .catch(error => {
          console.log("error: " + error)
        });
      if (add) {
        continue;
      }
      body.pendingTasks.splice(i, 1);
    }
    response.status(201).json({
      message: "OK",
      data: retval
    });
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        data: []
      })
    })
};

exports.findUsers = (request, response) => {
  let queries = url.parse(request.url, true);
  if (queries.search) {
    let result = User.find();
    let condition = queries.query;
    parseQuery(condition, result);
    result.exec().then(retval => {
      if (retval === null || retval.length === 0) {
        response.status(404).json({
          message: "User Not Found",
          data: []
        });
        return;
      }
      response.status(200).json({
        message: "OK",
        data: retval
      });
    })
      .catch(error => {
        response.status(500).json({
          message: "Server Error" + error,
          data: []
        });
      });
    return;
  }
  let result = User.find();
  result.exec().then(retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "User Not Found",
        data: []
      });
      return;
    }
    response.status(200).json({
      message: "OK",
      data: retval
    })
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        data: []
      });
    })
};

exports.createTask = async (request, response) => {
  let body = request.body;
  if(body.name === null || body.name === "") {
    response.status(403).json({
      message: "Task cannot be created (or updated) without a name",
      data: []
    });
    return;
  }
  if(body.deadline === null || body.deadline === "") {
    response.status(403).json({
      message: "Task cannot be created (or updated) without a deadline",
      data: []
    });
    return;
  }
  if (body.assignedUser !== null && body.assignedUser !== "") {
      await User.findById(body.assignedUser).exec().then(retval => {
      if (retval === null) {
        body.assignedUser = "";
        body.assignedUserName = "unassigned";
      } else {
        body.assignedUser = retval._id;
        body.assignedUserName = retval.name;
      }
      })
      .catch(error => {
        body.assignedUser = "";
        body.assignedUserName = "unassigned";
      })
  }
  let task = new Task(body);
  var rflag = false;
  task.save().then(async retval =>  {
    if (retval.assignedUserName !== "unassigned" && !retval.completed){
        await User.findByIdAndUpdate(retval.assignedUser, { $addToSet: {pendingTasks: retval._id}}, {new: true}).exec().then(wev => {}).catch(error => {
        rflag = true;
        response.status(500).json({
          message: "Server Error: " + error,
          doc: []
        })
      })
    }
    if (rflag) {
      return;
    }
    response.status(201).json({
      message: "OK",
      data: retval
    });
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        doc: []
      })
    })
};

exports.searchTasks = (request, response) => {
  let queries = url.parse(request.url, true);
  if (queries.search) {
    let result = Task.find();
    parseQuery(queries.query, result);
    result.exec().then(retval => {
      if (retval === null || retval.length === 0) {
        response.status(404).json({
          message: "Tasks Not Found",
          data: []
        });
        return;
      }
      response.status(200).json({
        message: "OK",
        data: retval
      });
    })
      .catch(error => {
        response.status(500).json({
          message: "Server Error: " + error,
          data: []
        });
      });
    return;
  }
  let result = Task.find();
  result.exec().then(retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "Task Not Found",
        data: []
      });
      return;
    }
    response.status(200).json({
      message: "OK",
      data: retval
    })
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        data: []
      });
    })
};

exports.searchUserID = (request, response) => {
  let id = request.params.id;
  User.findById(id).exec().then(retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "No User with id:" + id,
        data: []
      });
      return;
    }
    response.status(200).json({
      message: "OK",
      data: retval
    });
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        data: []
      });
    });
};

exports.updateUserID = async (request, response) => {
  let id = request.params.id;
  let body = request.body;
  if(body.name === null || body.name === "") {
    response.status(403).json({
      message: "Users cannot be created (or updated) without a name",
      data: []
    });
    return;
  }
  if(body.email === null || body.email  === "") {
    response.status(403).json({
      message: "Users cannot be created (or updated) without a email",
      data: []
    });
    return;
  }
  if (body.pendingTasks) {
    body.pendingTasks = body.pendingTasks.filter((v, i, a) => a.indexOf(v) === i);
  }
  User.findByIdAndUpdate(id, body, {new: true}).exec().then(retval => {
    Task.updateMany({assignedUser: id}, {assignedUserName: retval.name}, {new: true}).exec().then(upval => {
      response.status(200).json({
        message: "OK",
        data: retval
      })
    })
      .catch(error => {
        response.status(404).json({
          message: "Server Error: " + error,
          data: []
        })
      })

  })
    .catch(error => {
      response.status(404).json({
        message: "Server Error: " + error,
        data: []
      })
    })
};


exports.deleteUserID = (request, response) => {
  let id = request.params.id;
  let body = request.body;
  User.findByIdAndDelete(id).exec().then(retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "No User with id: " + id,
        data: []
      });
      return;
    }
    Task.updateMany({assignedUser: id}, {assignedUserName: "unassigned", assignedUser: ""}, {new: true}).exec().then(upval => {
      response.status(200).json({
        message: "OK",
        data: retval
      })
    })
      .catch(error => {
        response.status(500).json({
          message: "Server Error: " + error + " . And cannot delete user with id: " + id,
          data: []
        })
      })

  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error + " . And cannot delete user with id: " + id,
        data: []
      })
    })
};


exports.searchTaskID = (request, response) => {
  let id = request.params.id;
  Task.findById(id).exec().then(retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "No Task with id:" + id,
        data: []
      });
      return;
    }
    response.status(200).json({
      message: "OK",
      data: retval
    });
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error,
        data: []
      });
    });
};

exports.updateTaskID = async (request, response) => {
  let id = request.params.id;
  let body = request.body;
  if(body.name === null || body.name === "") {
    response.status(403).json({
      message: "Task cannot be created (or updated) without a name",
      data: []
    });
    return;
  }
  if(body.deadline === null || body.deadline  === "") {
    response.status(403).json({
      message: "Task cannot be created (or updated) without a deadline",
      data: []
    });
    return;
  }
  let old_val = 0
  await Task.findById(id).then(rv => {old_val = rv} ).catch(oe => {});
  Task.findByIdAndUpdate(id, body, {new: true}).exec().then(async retval => {
    let update_flag = true;
    if (old_val && old_val.assignedUser !== null) {
      await User.findByIdAndUpdate(old_val.assignedUser, {$pull: {pendingTasks: retval._id}}, {new: true}).exec().then(fuseval => {}).catch(error => {
        update_flag = false;
        response.status(500).json({
          message: "Server Error: " + error,
          data: []
        })
      });
    }
    if (retval.assignedUser !== null) {
      await User.findByIdAndUpdate(retval.assignedUser, {$addToSet: {pendingTasks: retval._id}}, {new: true}).exec().then(fuseval => {}).catch(error => {
        update_flag = false;
        response.status(500).json({
          message: "Server Error: " + error,
          data: []
        })
      });
    }
    if (retval.completed && retval.assignedUser !== null){
      await User.findByIdAndUpdate(retval.assignedUser, {$pull: {pendingTasks: retval._id}}, {new: true}).exec().then(fuseval => {}).catch(error => {
        update_flag = false;
        response.status(500).json({
          message: "Server Error: " + error,
          data: []
        })
      });
    }
    if (update_flag) {
      response.status(200).json({
        message: "OK",
        data: retval
      })
    }
  })
    .catch(error => {
      response.status(404).json({
        message: "Server Error: " + error + ". And cannot update task with id: " + id,
        data: []
      })
    })
};

exports.deleteTaskID = (request, response) => {
  let id = request.params.id;
  Task.findOneAndDelete(id).exec().then( async retval => {
    if (retval === null || retval.length === 0) {
      response.status(404).json({
        message: "No task with id: " + id + " found",
        data: []
      });
      return;
    }
    let rflag = false;
    if (retval.assignedUser) {
      await User.findByIdAndUpdate(retval.assignedUser, {$pull: {pendingTasks: retval._id}}).exec().catch(error => {
        rflag = true;
        response.status(500).json({
          message: "Server Error: " + error + ". And cannot delete task with id: " + id,
          data: []
        })
      })
    }
    if (rflag) {
      return;
    }
    response.status(200).json({
      message: "OK",
      data: retval
    })
  })
    .catch(error => {
      response.status(500).json({
        message: "Server Error: " + error + ". And cannot delete task with id: " + id,
        data: []
      });
    });
};