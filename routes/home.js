var secrets = require('../config/secrets');

module.exports = function (router) {
    const operations = require("../operation/index");
    var homeRoute = router.route("/");
    homeRoute.get(function (req, res) {
        var connectionString = secrets.token;
        res.json({ message: 'My connection string is ' + connectionString });
    });
    var usersRoute = router.route("/users");
    usersRoute.post(operations.createUser);
    usersRoute.get(operations.findUsers);

    var tasksRoute = router.route("/tasks");
    tasksRoute.post(operations.createTask);
    tasksRoute.get(operations.searchTasks);

    var userIdRoute = router.route("/users/:id");
    userIdRoute.get(operations.searchUserID);
    userIdRoute.put(operations.updateUserID);
    userIdRoute.delete(operations.deleteUserID);

    var taskIdRoute = router.route("/tasks/:id");
    taskIdRoute.get(operations.searchTaskID);
    taskIdRoute.put(operations.updateTaskID);
    taskIdRoute.delete(operations.deleteTaskID);

    return router;
};


